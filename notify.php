<?php  
include __DIR__ . '/Expo.php';
include __DIR__ . '/ExpoRegistrar.php';
include __DIR__ . '/ExpoRepository.php';
include __DIR__ . '/Repositories/ExpoFileDriver.php';
include __DIR__ . '/Exceptions/ExpoException.php';
include __DIR__ . '/Exceptions/ExpoRegistrarException.php';

header('Access-Control-Allow-Origin: *');

$deviceId = $_POST['deviceId'];
$token = $_POST['token'];
$body = $_POST['body'];
$jsonData = $_POST['data'];

if(!isset($token)){
	echo "OPTIONS";
}

  $interestDetails = [$deviceId, $token];
  
  // You can quickly bootup an expo instance
  $expo = \ExponentPhpSDK\Expo::normalSetup();
  
  // Subscribe the recipient to the server
  $expo->subscribe($interestDetails[0], $interestDetails[1]);
  
  // Build the notification data
  $notification = ['body' => $body, 'data' => $jsonData];
  
  // Notify an interest with a notification
  $expo->notify($interestDetails[0], $notification);